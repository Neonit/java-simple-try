package nl.neonit.monad;

import java.util.function.Function;

public class Failure<T> extends Try<T> {

    private final Throwable t;

    Failure(Throwable t) {
        this.t = t;
    }

    @Override
    public <U> Try<U> map(Function<T, U> f) {
        return Try.failure(t);
    }

    @Override
    public <U> Try<U> flatMap(Function<T, Try<U>> f) {
        return Try.failure(t);
    }

    @Override
    public <U> Try<T> accept(Function<T, U> f) {
        return Try.failure(t);
    }

    @Override
    public T get() {
        throw unchecked(t);
    }

    @Override
    public boolean isSuccess() {
        return false;
    }

    @Override
    public boolean isFailure() {
        return true;
    }

    private static RuntimeException unchecked(Throwable t) {
        if (t instanceof RuntimeException) {
            return (RuntimeException) t;
        }
        return new RuntimeException(t);
    }
}
