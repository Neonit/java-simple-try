package nl.neonit.monad;

import java.util.function.Function;

public class Success<T> extends Try<T> {

    private final T value;

    Success(T value) {
        this.value = value;
    }

    @Override
    public <U> Try<U> map(Function<T, U> f) {
        try {
            return new Success<>(f.apply(value));
        } catch (Throwable t){
            return Try.failure(t);
        }
    }

    @Override
    public <U> Try<U> flatMap(Function<T, Try<U>> f) {
        try {
            return f.apply(value);
        } catch (Throwable t){
            return Try.failure(t);
        }
    }

    @Override
    public <U> Try<T> accept(Function<T, U> f) {
        try {
            f.apply(value);
            return success(value);
        } catch (Throwable t){
            return Try.failure(t);
        }
    }

    @Override
    public T get() {
        return value;
    }

    @Override
    public boolean isSuccess() {
        return true;
    }

    @Override
    public boolean isFailure() {
        return false;
    }
}
