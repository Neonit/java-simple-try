package nl.neonit.monad;

import java.util.function.Function;
import java.util.function.Supplier;

public abstract class Try<T> {

    public static <U> Try<U> of(Supplier<U> f) {
        try {
            return Try.success(f.get());
        } catch (Throwable t) {
            return Try.failure(t);
        }
    }

    public abstract <U> Try<U> map(Function<T, U> f);

    public abstract <U> Try<U> flatMap(Function<T, Try<U>> f);

    public abstract <U> Try<T> accept(Function<T, U> f);

    public static <U> Try<U> success(U u) {
        return new Success<>(u);
    }

    public static <U> Try<U> failure(Throwable t) {
        return new Failure<>(t);
    }

    public abstract T get();

    public abstract boolean isSuccess();

    public abstract boolean isFailure();
}
