package nl.neonit.monad;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TryTest {

    private Try<String> successTry = Try.of(() -> "test");
    private Try<String> failTry = Try.of(() -> {throw new RuntimeException("oops");});

    @Test
    public void success() {
        assertTrue(successTry.isSuccess());
        assertFalse(successTry.isFailure());
        assertEquals("test", successTry.get());
    }

    @Test
    public void failure() {
        assertFalse(failTry.isSuccess());
        assertTrue(failTry.isFailure());
        assertThrows(RuntimeException.class, () -> failTry.get(), "oops");
    }


}