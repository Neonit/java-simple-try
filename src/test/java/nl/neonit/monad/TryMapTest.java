package nl.neonit.monad;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TryMapTest {

    private Try<String> successTry = Try.of(() -> "test");
    private Try<String> failTry = Try.failure(new RuntimeException("(╯°□°）╯︵ ┻━┻"));

    @Test
    public void mapOnSuccess() {
        Try<String> result = successTry.map(s -> s + "test");
        assertTrue(result.isSuccess());
        assertFalse(result.isFailure());
        assertEquals("testtest", result.get());
    }

    @Test
    public void mapOnFailure() {
        Try<String> result = failTry.map(s -> s + "test");
        assertTrue(result.isFailure());
        assertFalse(result.isSuccess());
    }
}
