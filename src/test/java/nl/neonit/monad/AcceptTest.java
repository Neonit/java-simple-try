package nl.neonit.monad;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class AcceptTest {

    private Try<String> successTry = Try.of(() -> "test");
    private Try<String> failTry = Try.failure(new RuntimeException("(╯°□°）╯︵ ┻━┻"));

    @Test
    void acceptOnSuccess() {
        ArrayList<String> strings = new ArrayList<>();
        Try<String> result = successTry.accept(s -> strings.add(s));
        assertTrue(result.isSuccess());
        assertFalse(result.isFailure());
        assertTrue(strings.contains(result.get()));
        assertEquals(1, strings.size());
    }

    @Test
    void acceptOnFailure() {
        ArrayList<String> strings = new ArrayList<>();
        Try<String> result = failTry.accept(s -> strings.add(s));
        assertTrue(result.isFailure());
        assertFalse(result.isSuccess());
        assertEquals(0, strings.size());
    }

}