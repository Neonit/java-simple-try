package nl.neonit.monad;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TryFlatMapTest {

    private Try<String> successTry = Try.of(() -> "test");
    private Try<String> failTry = Try.failure(new RuntimeException("(╯°□°）╯︵ ┻━┻"));

    @Test
    void flatMapOnSuccess() {
        Try<String> result = successTry.flatMap(s -> Try.of(() -> "tset"));
        assertTrue(result.isSuccess());
        assertFalse(result.isFailure());
        assertEquals("tset", result.get());
    }

    @Test
    void flatMapOnFailure() {
        Try<String> result = failTry.flatMap(s -> Try.of(() -> "tset"));
        assertFalse(result.isSuccess());
        assertTrue(result.isFailure());
    }
}